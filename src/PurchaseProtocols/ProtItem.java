package PurchaseProtocols;

public class ProtItem {
    public PurchaseProtocolData purchaseProtocolData;
    public PurchaseProtocolOSZData purchaseProtocolOSZData;
    public PurchaseProtocolPAAEData purchaseProtocolPAAEData;
    public PurchaseProtocolPAAE94FZData purchaseProtocolPAAE94FZData;
    public PurchaseProtocolPAEPData purchaseProtocolPAEPData;
    public PurchaseProtocolPAOAData purchaseProtocolPAOAData;
    public PurchaseProtocolRZ1AEData purchaseProtocolRZ1AEData;
    public PurchaseProtocolRZ2AEData purchaseProtocolRZ2AEData;
    public PurchaseProtocolRZAEData purchaseProtocolRZAEData;
    public PurchaseProtocolRZOAData purchaseProtocolRZOAData;
    public PurchaseProtocolRZOKData purchaseProtocolRZOKData;
    public PurchaseProtocolVKData purchaseProtocolVKData;
    public PurchaseProtocolZKData purchaseProtocolZKData;
    public ProtocolCancellationData protocolCancellationData;
    public ProtItem() {
    }
}
