package Protocols223;

public enum TypeProt223 {
    purchaseProtocol,
    purchaseProtocolIP,
    purchaseProtocolOSZ,
    purchaseProtocolPA_AE,
    purchaseProtocolPA_OA,
    purchaseProtocolPAAE,
    purchaseProtocolPAAE94,
    purchaseProtocolPAEP,
    purchaseProtocolPAOA,
    purchaseProtocolRKZ,
    purchaseProtocolRZ1AE,
    purchaseProtocolRZ2AE,
    purchaseProtocolRZ_AE,
    purchaseProtocolRZ_OA,
    purchaseProtocolRZ_OK,
    purchaseProtocolRZAE,
    purchaseProtocolRZOA,
    purchaseProtocolRZOK,
    purchaseProtocolVK,
    purchaseProtocolZK,
    purchaseProtocolCancellation
}
